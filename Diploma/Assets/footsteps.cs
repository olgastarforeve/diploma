﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class footsteps : MonoBehaviour

{
    public FMODUnity.EventReference footstepsevent;
    public LayerMask lm;
    public float typeSurface;


    void run()
    {
        MaterialCheck();
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.setParameterByName("surface_type", typeSurface);
        eventInstance.start();
        eventInstance.release();
        
        //FMODUnity.RuntimeManager.PlayOneShotAttached(footstepsevent, gameObject);
    }


    void MaterialCheck()
    {

        RaycastHit luch;
        
        if (Physics.Raycast(transform.position, Vector3.down, out luch, 0.3f, lm))
        {
            Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("rGround")) typeSurface = 0;
            else if (luch.collider.CompareTag("rGrass")) typeSurface = 1;
            else if (luch.collider.CompareTag("rRock")) typeSurface = 2;
            else if (luch.collider.CompareTag("rPuddle")) typeSurface = 3;
            //else typeSurface = 0;
        }
        
    }
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
        //footstepsinstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
